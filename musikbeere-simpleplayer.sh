#!/bin/sh

echo "SimplePlayer started!"
cd /music

while [ 0 ]
do
#  for f in *.ogg *.mp3
#  do
#    echo "Playing $f"
#    ogg123 -q "$f"
#  done
  for f in *.ogg *.mp3
  do
    echo "Playing $f"
    echo "$f" | grep -e ".ogg$" > /dev/null
    if [ $? -eq 0 ]
    then
      #OGG file
      ogg123 -q "$f"
    else
      echo "$f" | grep -e ".mp3$" > /dev/null
      if [ $? -eq 0 ]
      then
        #MP3 file
        madplay -q "$f"
      fi
    fi
  done
done

echo "SimplePlayer exited infinite loop!"
